#!/usr/bin/python
# -*- coding: utf-8 -*-

# Mia dumtempa konvencio:
#
# - ikso-metodo por variabloj
# - ikso-metodo por montrotaj tekstoj;
# - ĉapeloj (UTF-8) por dosieroj kreotaj;
# - ĉapeloj (UTF-8) por komentoj.
#
# Tio povas ŝanĝiĝi depende de la subteno al UTF-8 de aliaj iloj.
# Ekz.: multaj redaktiloj subtenas UTF-8, do oni povas krei tiajn dosierojn kaj skribi ĉapelojn en komentoj senprobleme.
# Tamen, Python 2.x ne permesas uzi ĉapelojn en nomoj de variabloj.
# La surekranaj mesaĝoj povas esti iksaj aŭ ĉapelaj, sed mi jam havis plurajn iksojn kiam mi decidis mian konvencion.
# Krome, estus pli facile uzi ĉi tiun programon en Windows, aŭ malnovaj, aŭ misagorditaj sistemoj se oni ne uzos ĉapelojn
# en surekranaj mesaĝoj (mi programas en Linukso, tamen).

# Por igi Python 2.x pli simila al Python 3.x 
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division # ne uzata, sed eble...

from BeautifulSoup import BeautifulSoup
from collections import OrderedDict
import re, os, sys, codecs, json, logging


# TODO: Farotaj taskoj:
# - Trakti u'\u00b7' (mezaltan punkton ·) kiel apartigilon de difinoj. Ĝi aperas pli ofte en la vortaro Portugala→Esperanto.
# - Testi la vortaron Portugala→Esperanto kaj adapti la programon por ĝi (la ĉefa testejo ĝis nun estas Esperanto→Portugala)
# - Plibonigi ekzemplojn (ekz. vidu ke la komenco de la traduko ne estas klara: "subtera  kablo cabo subterrâneo")
# - En la antaŭa ekzemplo, vidu ankaŭ ke estas du spacetoj inter "subtera" kaj "kablo". Devas esti nur unu.
# - Plibonigi rimarkojn (nur unu estas apartigita)
# - Korekti la avertojn (ili aperas kiel WARNING sur la ekrano).
# - Ŝanĝi SimplaVortaro-n por montri difinojn en maniero pli simila al la ekzistanta VTF
# - Kaj aliaj aferoj menciitaj en la "docstring" de aldoniVortojnAlDict

if __name__ == "__main__":
    # Elektu ĉi tie kiom da informoj vi deziras vidi sur la ekrano
    # DEBUG = detaloj por korekti/kompreni la programon, informoj, avertoj, kaj pli gravaj (se ekzistas)
    # INFO = informoj, avertoj, kaj pli gravaj (se ekzistas)
    # WARN = nur avertoj kaj pli gravaj (se ekzistas)

    #deziratajInformoj = logging.DEBUG
    #deziratajInformoj = logging.INFO
    deziratajInformoj = logging.WARN
    logging.basicConfig(level = deziratajInformoj)



def apartigiDifinojn(vortoKunDifino):
    """Ĉi tiu funkcio serĉas numerojn (1, 2,..., 9) por apartigi difinojn.
    La rezulto estas listo, kie la unua (t.e. [0]) elemento estas la vorto
    kaj la aliaj estas la difinoj"""
    
    # TODO: forigi la numerojn ĉar SimplaVortaro jam numerigas la erojn.

    # Se vi volas uzi la HTML-an strukturon por trovi utilajn informojn, ĉi tie
    # estas la lasta oportuno, ĉar ni forigos ĉiom da HTML!

    supeto = BeautifulSoup(vortoKunDifino)

    trovitajNumeroj = []
    for tagB in supeto.findAll('b'):
        trovitajNumeroj += tagB.findAll(text = re.compile(r'\b\d\b'))

    if len(trovitajNumeroj) == 0:
        nurEnhavo = fNuda(vortoKunDifino)
        return nurEnhavo.split(None, 1) # apartigi je la unua spaco, konsideri plurajn spacojn kiel unu
    else:
        for trov in trovitajNumeroj:
            (rezulto, kvanto) = re.subn(r'\b(\d)\b', '\x1F\\1', trov)
            if kvanto != 1:
                logging.warn('Mi atendis nur unu numeron, sed trovis %d en %s', kvanto, vortoKunDifino)
            trov.replaceWith(rezulto) # meti markojn \x1F (unit separator) antaŭ la numeroj


        nurEnhavo = fNuda(unicode(supeto)) # forigi HTML-on

        # TODO: Se mi trovos u'\u00b7' (mezaltan punkton ·) iom antaŭ la numero
        # tio signifas ke ekzistas (eble sen-parenteza) rimarko antaŭ la difino.
        # Mi devas movi la apartigilon '\x1F' (unit separator) al la ĝusta loko.
        rezulto = nurEnhavo.split('\x1F') # apartigi tekston laŭ la markoj

        rezultoRe = re.search(r'(\(.+?\)\s*)$', rezulto[0])
        if rezultoRe is not None: # Ekzistas rimarko antaŭ la unua difino
            # TODO: Kio se la rimarko antaŭ la numero ne estas inter parentezoj?
            rezulto[0] = rezulto[0][0 : rezultoRe.start()]
            rezulto[1] = rezultoRe.group(1) + rezulto[1]
        rezulto[0] = rezulto[0].strip()
        return rezulto

def aldoniVortojnAlDict(dic, radiko, listoDeVortoj, ekzemploj):
    """Ĉi tiu funkcio aldonas la vortojn (kun difinojn) de listoDeVortoj al la vortaro dic,
    ĉiuj ligitaj al la radiko (kiu ne estos aldonita memstare al dic).
    
    La rezulto estas dic mem.
    
    La formato de dic estas tiu uzata de "SimplaVortaro.org".
    
    Ekzistas kelkaj aferoj kiuj ne kongruas inter Vortaro Túlio Flores (VTF) kaj SimplaVortaro (SV):
    - En VTF, la ekzemploj aperas sub la radiko/ĉefvorto (kiel tutaĵo),
    dume en SV la ekzemploj povas aperi apud ĉiu difino aŭ vorto.
    - SV ebligas mencii fonton de ekzemplo ("examples": [[ekz, fonto], [ekz, fonto]]),
    sed VTF ne havas tion.
    - SV ebligas havi plurajn difinojn (1, 2, 3) kaj subdifinojn (1.a, 1.b, 1.c). Ŝajnas
    ke mi ne uzos subdifinojn.
    - VTF havas plurajn rimarkojn: (tb.), Vd., adj., (tr.), (i.), (zool.) kaj mi ne certas ĉu ekzistas
    plene bona maniero organizi ilin en "remarks" (eble sufiĉas lasi ilin en la difinaro, ĉu?).
    - Kelkaj rimarkoj aplikeblas al pluraj difinoj,
    ekz.: malkaŝi (tr.) 1 ....... 2 .......
    jurássico adj. 1 (geol.: .......) ....... · s.m. 2 (geol.: .......) ....... 
    - VTF povas havi multe da informo sub unu radiko (ekz. kaŝi havas multajn derivitajn vortojn
    kaj ekzemplojn) sed SV preferas montri nur la difinon, kiu estis serĉita plus kelkajn derivaĵojn,
    kiujn ĝi kalkulas aŭtomate laŭ simileco. Mi pensas ke estas bona ideo modifi SV por ligi la
    derivaĵojn kaj la ĉefan vorton. Bonŝance la programlingvo de SV kaj Apartigi estas la sama :-)
    """
    # Mi serĉas nur unu rimarkon... Ĉu indas serĉi aliajn?
    # Ĉu indas kontroli ĉu la teksto estas <i>klinita</i>?
    # Kio se la rimarko estas post la numero? Eble mi povas forigi la numerojn antaŭe,
    # ĉar estas facile remeti ilin.
    rimarkojRe = re.compile(r'^\s*(\(.+?\))')
    cxuCxefa = True
    for vorto in listoDeVortoj:

        apartigitajDifinoj = apartigiDifinojn(vorto)
        if len(apartigitajDifinoj) == 0:
            logging.warn('Mi trovis malplenan vorton sub la radiko %s', radiko)
            continue

        vorto = apartigitajDifinoj[0]
        difinaro = apartigitajDifinoj[1:]
        originalaVorto = vorto
        vorto2 = None
        i = 0
        while vorto in dic:
            i += 1
            vorto2 = originalaVorto + ' (' + unicode(i) + ')'
            logging.warn('Mi trovis ekzistantan difinon de %s. Mi provos uzi %s', vorto, vorto2)
            vorto = vorto2
        if vorto2 is not None:
            logging.warn('Do mi decidis aldoni %s', vorto)

        rimarkaro = []
        for i, difino in enumerate(difinaro):
            rezultoRe = rimarkojRe.search(difino)
            if rezultoRe is not None:
                rimarkaro.append([rezultoRe.group(1)])
                difinaro[i] = difinaro[i][rezultoRe.end():].strip()
            else:
                rimarkaro.append([])
        dic[vorto] = {
            "root": radiko,
            "primary": cxuCxefa,
            "definitions": [
                {
                    "primary definition": difino,
                    "remarks": rimarkoj,
                    "examples": [[ekz, None] for ekz in ekzemploj if cxuCxefa and not ekz.isspace()],
                    "subdefinitions": [],
                    "translations": {}
                } for (difino, rimarkoj) in zip(difinaro, rimarkaro)
            ]
        }
        cxuCxefa = False
    return dic

def fNuda(htmla):
    listo1 = re.findall(r'>[^<]+<', htmla)
    nudListo = [e[1:-1] for e in listo1]
    return ''.join(nudListo)

def partoj(p):
    """Ĉi tiu funkcio donas liston kiu enhavas la sekvajn fratojn (nextSibling's)
    de la nodo p (objekto de BeautifulSoup)"""
    vPartoj = []
    etik = p
    vPartoj.append(unicode(etik))
    while etik:
        etik = etik.nextSibling
        vPartoj.append(unicode(etik))
    return vPartoj

# def utf8(objekto):
#     #print 'En la funkcio utf8, la tipo de la objekto estas:', type(objekto) 
#     return str(objekto).decode('utf-8')

# Elektu la plej bonan lokon por voki ĉi tion.
# Ĉu komence aŭ fine? Aŭ neniam?
# def forigiMalplenajnIkajB(supo):
#     # forigi <b> kaj <i>, kiuj enhavas nur spacojn
#     for bi in supo.findAll('b') + supo.findAll('i'):
#         if bi.getString() and bi.getString().isspace():
#             bi.replaceWithChildren()
#
# def forigiNeutilajnSpan(supo):
#     #print 'antaŭe:', supo.prettify()
#     # forigi neutilajn <span> (t.e. span sen atributoj)
#     for span in supo.findAll('span'):
#         if not span.attrs:
#             span.replaceWithChildren()
#     #print 'poste:', supo.prettify()

def purigiHtml(dosierTeksto):
    # La suba simpligo estas utila ĉefe por legi pli facile
    # la tekstojn kiujn ĉi tiu programo skribas sur la ekrano per logging.debug().
    # Multe (aŭ ĉiom da) logiko de la programo ne zorgas pri nenokataj
    # atributoj kiel mso-bidi-*.

    # mso-bidi, for de tie ĉi!
    dosierTeksto = re.sub(r'mso-bidi-font-size:\s*12\.0pt;?', '', dosierTeksto)
    dosierTeksto = re.sub(r'mso-bidi-font-weight:\s*normal;?', '', dosierTeksto)
    dosierTeksto = re.sub(r'mso-bidi-font-weight:\s*bold;?', '', dosierTeksto)
    dosierTeksto = re.sub(r'mso-bidi-font-style:\s*normal;?', '', dosierTeksto)
    dosierTeksto = re.sub(r'mso-bidi-font-style:\s*italic;?', '', dosierTeksto)
    dosierTeksto = re.sub(r"""\s?style=['"]\s*['"]""", '', dosierTeksto)
    #dosierTeksto = re.sub('\x93', '"', dosierTeksto) # malferma citilo
    #dosierTeksto = re.sub('\x94', '"', dosierTeksto) # ferma citilo
    dosierTeksto = re.sub('\n', ' ', dosierTeksto)
    dosierTeksto = re.sub('\r', ' ', dosierTeksto)
    return dosierTeksto

def forigiTb(partojP, kieEkzistasRadiko):
    """Ĉi tiu funkcio serĉas la partojn, kiuj enhavas aŭ estas inter "(tb." kaj ")".
    Se la radiko troviĝas en unu el tiuj partoj, tiu parto estos forigita el kieEkzistasRadiko."""
    tbOkej = False
    komencoTbRe = re.compile('\(.*tb\.', re.IGNORECASE)
    finoTbRe = re.compile(r'\)')
    for i, e in enumerate(partojP):
        if tbOkej and finoTbRe.search(e):
            tbOkej = False
        if komencoTbRe.search(e):
            tbOkej = True
        if tbOkej and i in kieEkzistasRadiko:
            kieEkzistasRadiko.remove(i)

def forigiVd(partojP, kieEkzistasRadiko):
    interneDeVd = False
    komencoVdRe = re.compile(r'Vd\.')
    # Bedaŭrinde la nura marko, kiun la vortaro havas por signi la finon de
    # la referenco "Vd.", estas la fina punkto (ekz. "Vd. kaŝkopii.")
    finoVdRe = re.compile(r'\.\s*')
    for i, e in enumerate(partojP):
        supo = BeautifulSoup(e)
        if interneDeVd and supo.findAll(text = finoVdRe):
            interneDeVd = False
        if supo.findAll(text = komencoVdRe):
            interneDeVd = True
        if interneDeVd and i in kieEkzistasRadiko:
            kieEkzistasRadiko.remove(i)

def forigiOu(partojP, kieEkzistasRadiko):
#     ouOkej = False
#     for i, e in enumerate(partojP):
#         if ouOkej and re.search(r'\)', e):
#         #if ouOkej and re.search(r'\.[ <]', e):
#             ouOkej = False
#         if re.search('\(ou ', e, re.I):
#             ouOkej = True
#         if ouOkej and i in kieEkzistasRadiko:
#             kieEkzistasRadiko.remove(i)
    interneDeOu = False
    komencoOuRe = re.compile(r'\(ou ', re.IGNORECASE)
    finoOuRe = re.compile(r'\)')
    for i, e in enumerate(partojP):
        supo = BeautifulSoup(e)
        if supo.findAll(text = komencoOuRe):
            interneDeOu = True
        # La testo de fino devas aperi post la testo de komenco.
        # Tio donas ĝustan rezulton se (ou ...) komenciĝas kaj finiĝas en la sama parto.
        # TODO: Ŝanĝi forigiTb kaj forigoVd sammaniere. Mi ne korektis ilin nun ĉar
        # tiaj taskoj estas pli bone faritaj kun testoj kaj zorgo. 
        if interneDeOu and supo.findAll(text = finoOuRe):
            interneDeOu = False
        if interneDeOu and i in kieEkzistasRadiko:
            kieEkzistasRadiko.remove(i)

def forigiEgalan(partojP, kieEkzistasRadiko):
    interneDeEgalajxo = False
    for i, e in enumerate(partojP):
        if re.search(r'\= ', e, re.IGNORECASE):
            interneDeEgalajxo = True
        if interneDeEgalajxo and i in kieEkzistasRadiko:
            kieEkzistasRadiko.remove(i)
        if interneDeEgalajxo and re.search(r'\.[ <]', e):
            interneDeEgalajxo = False

def troviLastanB(teksto):
    # Mi kredas ke oni povas simpligi ĉi tiun funkcion
    # se oni ne bezonos ke tiom da detaloj aperu sur la ekrano. 
    dikaObj = re.finditer(r'<b.*?</b>', teksto, re.IGNORECASE)
    dikaPoz = []
    dikaEnhavo = []
    for e in dikaObj:
        dikaPoz.append(e.start())
        dikaEnhavo.append(e.group(0))
    logging.debug("Mi kreis la liston dikaEnhavo, kun: %s", dikaEnhavo)
    logging.debug("Kaj mi konservis la poziciojn de tiuj eroj en la listo dikaPoz: %s", dikaPoz)
    if not dikaPoz or not dikaEnhavo:
        logging.warn('La funkcio "troviLastanB ne trovis <b>...</b>. Kontrolu: %s', teksto)
        return None, None
    return dikaPoz[-1], dikaEnhavo[-1]

# Lingva rimarko: kelkaj vortaroj preferas "proceso" nur por juraj aferoj, kaj preferas "procezo" alikaze
# Tamen, ili agnoskas ke oni ja povas uzi "proceso" por ĉiuj signifoj.
# Pro tio ke nur "proceso" aperas en la akademia vortaro, mi preferis uzi "procesi" anstataŭ "procezi".
def procesiEkzemploListon(radiko, ekzemploListo):
    """Ĉi tiu funkcio transformas liston de ekzemploj tiel ke <b>esprimo</b>, kiuj komencas ekzemplojn,
    troviĝos ĉiam komence de elemento (en la formoj <b>esprimo</b> traduko aŭ <span><b>esprimo</b></span> traduko).
    Oni volas eviti la jenan strukturon en unu elemento de la listo: traduko <b>esprimo</b> traduko.
    La rezulto de ĉi tiu funkcio estas la transformita listo. La pasita listo ne estos ŝanĝita.
    Rimarku ke ja restos kelkaj ne-naturaj rompoj en la strukturo de la listo."""
    logging.debug("\n--Procesado de ekzemploj--")
    
    # La strukturo de ĉi tiu funkcio estas tre simila al la strukturo de la ĉefa apartigilo en la funkcio
    # "procesiDosieron". Fakte, ili estas koncepte egalaj, ili nur diferencas je detaletoj de realigo.
    # Kvankam iom da kodo estas nun ripetita, mi multe lernis dum mi rekreis ĉi tiun algoritmon,
    # kaj mi pensas ke estontece eblos/necesos evoluigi la algoritmojn en malsamajn direktojn.
    # Krome, la duparta procesado (difinoj kaj ekzemploj) unue ne ekzistis. Ĉu mi devas unuigi denove
    # tiun procesadon aŭ lasi ilin evolui aparte? Mankas la unuigo de kelkaj strukturoj (Ĉu list aŭ set? 
    # Ĉu cikli tra la tuta listo kaj "continue" tiam kiam ne ekzistas radiko aŭ cikli nur tra
    # la lokoj kie ekzistas radiko?)

    ekzListo = ekzemploListo[:] # Mi volas _ne_ modifi la pasitan parametron
    
    cxuEkzistasRadikoRe = re.compile(r'<b.*?>.*?' + re.escape(radiko) + r'.*?</b>', re.IGNORECASE)
    kieEkzistasRadiko = set(idx for idx, ekz in enumerate(ekzListo) if cxuEkzistasRadikoRe.search(ekz))
    # Bonŝance Python permesas al mi uzi kaj list-on kaj set-on tute egale (duck typing), dum mi provas
    # tiujn malsamajn datenstrukturojn.
    forigiTb(ekzListo, kieEkzistasRadiko)
    forigiVd(ekzListo, kieEkzistasRadiko)
    forigiOu(ekzListo, kieEkzistasRadiko)
    forigiEgalan(ekzListo, kieEkzistasRadiko)
    fermiBSpanRe = re.compile(r'</b>.*?</span>', re.IGNORECASE)
    for i, nunaEkzemplo in enumerate(ekzListo):
        # Kontroli ĉu oni vere povas ignori la unuan elementon de la listo. Ĉu ĝi ĉiam estas malplena teksto?
        if i == 0: # Mi bezonas ignori tion ĉefe pro la ofta esprimo i-1 en la jena kodo.
            continue

        if i not in kieEkzistasRadiko:
            continue
        
        if fermiBSpanRe.search(nunaEkzemplo):
            # Mi trovis ke ekzistas io simila al "<span>{ekzemploTraduko} <b>ekzemplo</b></span> {ekzemploTraduko}".
            # La unua traduko estas de la antaŭa ekzemplo.
            logging.debug("Mi trovis </b>.*?</span> en la nuna ekzemplo (i = %d)", i)
            dikaKomenco, _ = troviLastanB(nunaEkzemplo)

            logging.debug('Mi trovis ke ero de %s fakte apartenas al la antauxa ekzemplo %s', ekzListo[i], ekzListo[i-1])
            ekzListo[i-1] = ekzListo[i-1] + ekzListo[i][:dikaKomenco] + '</span>'
            ekzListo[i] = '<span>' + ekzListo[i][dikaKomenco:]
            logging.debug("Kaj do mi kreis ekzListo[i-1] %s kaj ekzListo[i] %s\n", ekzListo[i-1], ekzListo[i])
            
        if ekzListo[i-1].endswith('b>') and i-1 not in kieEkzistasRadiko:
            logging.debug("La antauxa parto finigxas per b>")
            logging.debug('Pro tio ke la valoro i-1 (%d) neniam okazas en la aro "kieEkzistasRadiko", mi aldonos gxin al la aro', i-1)
            kieEkzistasRadiko.remove(i)
            kieEkzistasRadiko.add(i-1)
        elif ekzListo[i-1].endswith('b></span>'):
            # Temas pri kazo kie ero de antaŭa parto fakte apartenas al la nuna parto.
            logging.debug("La antauxa ekzemplo finigxas per b></span>. Do gxi fakte apartenas al la nuna ekzemplo.")
            dikaKomenco, dikaEnhavo = troviLastanB(ekzListo[i-1])
            if dikaKomenco is not None and dikaEnhavo is not None:
                ekzListo[i-1] = ekzListo[i-1][:dikaKomenco] + '</span>'
                ekzListo[i] = '<span>' + dikaEnhavo + ekzListo[i]
                logging.debug("Kaj fine el tio mi kreis ekzListo[i-1] %s kaj ekzListo[i] %s", ekzListo[i-1], ekzListo[i])
            
    # <b>dika teksto</b> kiu ne enhavas la radikon probable estas nur finaĵo (i, oj). Oni devas kunigi ĝin
    # kun la antaŭa elemento.

    rezultaEkzemploListo = []
    for i, ekz in enumerate(ekzListo):
        if i in kieEkzistasRadiko:
            rezultaEkzemploListo.append(ekz)
        else:
            if rezultaEkzemploListo:
                rezultaEkzemploListo[-1] += ekz
            else:
                # La rezulta listo estas ankoraŭ malplena.
                # Ĉi tio okazos por la unua elemento.
                rezultaEkzemploListo.append(ekz)
    
    # Se la unua ero ne enhavas la radikon, krocxu la sekvajn erojn
    # por ke tiel estu.
    while len(rezultaEkzemploListo) >= 2 and not cxuEkzistasRadikoRe.search(rezultaEkzemploListo[0]):
        rezultaEkzemploListo[0] += rezultaEkzemploListo[1]
        rezultaEkzemploListo[1:] = rezultaEkzemploListo[2:]
        
    
    logging.debug("--Fino de procesado de ekzemploj--")
    return rezultaEkzemploListo


def procesiDosieron(dosierNomo):
    # Mi jam scias ke la fonta dosiero estas en la kodo cp1252
    # Nature, pli evoluinta programo akceptos aliajn kodojn.
    # BeautifulSoup ankaŭ povas trakti la signan enkodigon aŭtomate, 
    # sed tio postulas kelkajn ŝanĝojn al la programo. 
    with codecs.open(dosierNomo, "r", "cp1252") as dosierObjekto:
        dosierTeksto = dosierObjekto.read()
    
    dosierTeksto = purigiHtml(dosierTeksto)    
    
    paragrafRe = re.compile(r'<p.*?><b.*?>.*?</p.*?>')
    paragrafVektoro = paragrafRe.findall(dosierTeksto)
    
    rezultDict = OrderedDict()
    nudaShablono = re.compile(r'>[^<]+<')
    
    ## kaf 56, kahelo 62, kanĝio 324, kanti 349, kaŝi 674, kvociento [-6:-5]
    #======================================================================================
    #======================================================================================
    for elementoP in paragrafVektoro: #[349:350]:#[674:675]: #[62:63]: #[6:5]: #[10:11]: #[56:57]:
        logging.info('---- Komenco de nova paragrafo ----')
        supo = BeautifulSoup(elementoP, convertEntities = BeautifulSoup.HTML_ENTITIES)
        supaB = supo.find('b')

        nuda = nudaShablono.search(unicode(supaB)).group(0)
        #nuda = re.search(r'>[^<]+<', elementoP).group(0)
        radiko = nuda[1:-1] # Mi forigas > de la komenco kaj < de la fino, tiam mi havas la radikon! Hura!
        radiko = radiko.strip()
        logging.info('La radiko estas: %s', radiko)

        #print 'supaB:', supaB.prettify()
        #print '------------'
    
        partojP = partoj(supaB)
        lastaParto = '<b>'+radiko+'</b>'
        #-----------------
        ekzemploKomencoIndico = None 
        ekzemploListo = []
        logging.debug("Mi trovis %d partojn.", len(partojP))
        for i, e in enumerate(partojP):
            logging.debug("Jen [%d]:", i)
            logging.debug("%s", e)
            if u'\u00a8' in e:
                ekzemploKomencoIndico = i
                logging.debug('Mi trovis la simbolon kiu komencas ekzemplajn frazerojn')
            #if u'||' in e:
            #    logging.debug('Mi trovis la simbolon kiu komencas ekzemplajn frazojn')
            #    # Foje necesos dividi ĉi tiun parton, ĉar "||" ne aperas ĉiam sola
            logging.debug('\n')
        logging.debug("Fino de la partolisto")

        if ekzemploKomencoIndico is None:
            #print 'partojP[-1]:', partojP[-1] # ŝajnas ke estas ĉiam None
            partojP[-1] = lastaParto
        else:
            ekzemploListo = partojP[ekzemploKomencoIndico+1:] 
            partojP = partojP[:ekzemploKomencoIndico]
            partojP.append(lastaParto)

        logging.debug("ekzemploListo antaŭ procesado: %s", ekzemploListo)
        ekzemploListo = procesiEkzemploListon(radiko, ekzemploListo)
        logging.debug("ekzemploListo post procesado: %s", ekzemploListo)

        kunradikoListo = [i for i, e in enumerate(partojP)
                          if re.search(r'<b.*?'+re.escape(radiko)+r'.*?</b>', e, re.IGNORECASE)]
        #-----------------
        logging.debug("En la jenaj pozicioj, mi trovis la radikon %s inter <b></b>:", radiko)
        logging.debug("%s", kunradikoListo)
    
        if not kunradikoListo.count(0):
            kunradikoListo.insert(0, 0)

        forigiTb(partojP, kunradikoListo)
        logging.debug("Post forigo de eroj kun (tb. ...), mi havas la jenan liston de pozicioj:")
        logging.debug("%s", kunradikoListo)
        
        forigiVd(partojP, kunradikoListo)
        logging.debug("Post forigo de eroj kun Vd. ...., mi havas la jenan liston de pozicioj:")
        logging.debug("%s", kunradikoListo)
        
        forigiOu(partojP, kunradikoListo)
        logging.debug("Post forigo de eroj kun (ou ...), mi havas la jenan liston de pozicioj:")
        logging.debug("%s", kunradikoListo)
        
        forigiEgalan(partojP, kunradikoListo)
        logging.debug("Post forigo de eroj kun = ...., mi havas la jenan liston de pozicioj:")
        logging.debug("%s", kunradikoListo)

        kunradikoKopio = kunradikoListo[1:]
        logging.debug("Mi cicklos tra %s", kunradikoKopio)
        fermiBSpanRe = re.compile(r'</b>.*?</span>', re.IGNORECASE)
        for k_i, k_e in enumerate(kunradikoKopio):
            nunaParto = partojP[k_e]
            antauxaParto = partojP[k_e-1]
            antauxaNudaj = nudaShablono.findall(antauxaParto, re.I)
            logging.debug("\n--Ciklo # %d, parto # %d--", k_i, k_e)
            logging.debug("La nuna parto estas %s kaj la antauxa parto estas %s", nunaParto, antauxaParto)
            logging.debug("La listo antauxaNudaj estas %s", antauxaNudaj)
            logging.debug("\n--Korektado--")
            if fermiBSpanRe.search(nunaParto):
                # Mi trovis ke ekzistas io simila al "{difino} <b>vorto</b> {difino}". La unua difino estas de la antaŭa vorto.
                # Ekzemplo reala, sed simpligita: <b>kahelo</b> {difino} <b>kahela</b> {difino}  
                logging.debug("Mi trovis </b>.*?</span> en la nunaParto")

                dikaKomenco, _ = troviLastanB(nunaParto)    
                logging.debug('Mi trovis ke ero de %s fakte apartenas al la antauxa parto %s', partojP[k_e], partojP[k_e-1])
                partojP[k_e-1] = partojP[k_e-1] + partojP[k_e][:dikaKomenco] + '</span>'
                partojP[k_e] = '<span>' + partojP[k_e][dikaKomenco:]
                logging.debug("Kaj do mi kreis partojP[k_e-1] %s kaj partojP[k_e] %s\n", partojP[k_e-1], partojP[k_e])
                #print "partojP igxis:", partojP

            if antauxaParto.endswith('b>'):
                if kunradikoListo.count(k_e-1) == 0:
                    # Reala ekzemplo, simpligita: antaŭaParto = '<b><span>lakto</span></b>'; nunaParto = '<b><span>kaf</span></b>'
                    logging.debug("La antauxa parto finigxas per b>")
                    nunaIndico = kunradikoListo.index(k_e) # Atentu: nunaIndico != k_i
                    logging.debug("La unua okazo de la valoro k_e (%d) en kunradikoListo estas en la pozicio %d", k_e, nunaIndico)
                    logging.debug("Pro tio ke la valoro k_e-1 (%d) neniam okazas en kunradikoListo, mi aldonos gxin en la pozicio %d", k_e-1, nunaIndico)
                    kunradikoListo[nunaIndico] = k_e-1
                    # Do, oni povas simple diri ke la radiko komenciĝas en la antaŭa parto...
                    # Tio estas interesa, ĉar en aliaj partoj de ĉi tiu funkcio la programo
                    # {faras | bezonas fari} pli kompleksajn tekst-manipuladojn...
                    logging.debug("kunradikoListo igxis: %s\n", kunradikoListo)
            elif antauxaParto.endswith('b></span>'):
                # Temas pri kazo kie ero de antaŭa parto fakte apartenas al la nuna parto.
                # Mi trovis ekzemplon en "kafo" -> "maltokafo"
                # -> <b>malto</b></span><b><span>kaf
                logging.debug("La antauxa parto finigxas per b></span>. Do gxi fakte apartenas al la nuna parto.")
                dikaKomenco, dikaAntauxaEnhavo = troviLastanB(antauxaParto)
                partojP[k_e-1] = partojP[k_e-1][:dikaKomenco] + '</span>'
                partojP[k_e] = '<span>' + dikaAntauxaEnhavo + partojP[k_e]
                logging.debug("Kaj fine el tio mi kreis partojP[k_e-1] %s kaj partojP[k_e] %s", partojP[k_e-1], partojP[k_e])
                #logging.debug("partojP igxis: %s\n", partojP)


        logging.debug("\n\n--Rezulto--")
        vortoj = []
        for i in range(len(kunradikoListo)-1):
            logging.debug("Mi krocxos la partojn de %s gxis %s (sen la lasta)", kunradikoListo[i], kunradikoListo[i+1])
            #for test_parto in partojP[kunradikoListo[i]:kunradikoListo[i+1]]:
            #    print 'La teksto', test_parto, 'havas la tipon', type(test_parto)
            vorto = ''.join(partojP[kunradikoListo[i]:kunradikoListo[i+1]])
            #supo = BeautifulSoup(vorto)
            #forigiMalplenajnIkajB(supo)
            #forigiNeutilajnSpan(supo)
            #print fNuda(vorto)
            logging.debug("vorto: %s\n", vorto)
            #print "purigita supo:", supo
            #vortoj.append(supo)
            vortoj.append(vorto)
        logging.info("Mi aldonos %d vorto(j)n de la radiko %s", len(vortoj), radiko)
        aldoniVortojnAlDict(rezultDict, radiko, vortoj, [fNuda(ekz).strip() for ekz in ekzemploListo])
    return rezultDict


if __name__ == "__main__":
    if len(sys.argv) < 2 or sys.argv[1] == '--help':
        print('Uzmaniero: %s «nomo-de-dosiero»' % sys.argv[0])
        sys.exit(1 if len(sys.argv) < 2 else 0) # se oni ne donis sufiĉajn argumentojn, la kodo estas 1 (eraro), alikaze (--help), ĝi estas 0 (sukceso)
    else:
        dosierNomo = sys.argv[1] #"ep_k.htm"
        rezultDict = procesiDosieron(dosierNomo)
        i = 1
        while True:
            rezultDosierNomo = dosierNomo + '.' + unicode(i) + '.json'
            if not os.path.exists(rezultDosierNomo):
                break
            i += 1
        # FIXME: "Race condition" inter ekzist-testo kaj kreo de la dosiero...
        # Oni diras ke os.open("x", os.O_WRONLY | os.O_CREAT | os.O_EXCL)
        # solvas tiun problemon (aŭ almenaŭ garantios ke oni ne anstataŭos ekzistantan dosieron)
        # Eble os.fdopen kaj codecs.EncodedFile estos utilaj ankaŭ.
        with codecs.open(rezultDosierNomo, 'w', 'utf-8') as rezultDosiero:
            json.dump(rezultDict, rezultDosiero, ensure_ascii = False, indent = 2)
        print('Mi skribis la rezulton en', rezultDosierNomo)
    
